#!/bin/bash

export PATH=$PATH:/qemu/bin/

qemu-system-x86_64 -m 2048 -smp 2 -chardev socket,id=SOCKSYZ,server,nowait,host=localhost,port=46514 -mon chardev=SOCKSYZ,mode=control -display none -serial stdio -no-reboot -name VM-test -device virtio-rng-pci -enable-kvm -cpu host,migratable=off -device e1000,netdev=net0 -netdev user,id=net0,restrict=on,hostfwd=tcp:127.0.0.1:28993-:22 -hda /images/bullseye.img -snapshot -kernel kernel/bzImage -append "root=/dev/sda console=ttyS0 net.ifnames=0 biosdevname=0"
