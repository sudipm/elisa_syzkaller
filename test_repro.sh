#!/bin/bash

set -ex

touch qemu.log
while true; do
	n=`cat qemu.log | grep "syzkaller login" | wc -l`
	if [ $n -gt 0 ]; then
		break;
	else
		sleep 1
	fi
done

scp -P 28993 -i /images/ssh.key -o StrictHostKeyChecking=no repro.c 127.0.0.1:~/.
ssh -p 28993 -i /images/ssh.key root@127.0.0.1 cc repro.c
ssh -p 28993 -i /images/ssh.key root@127.0.0.1 ~/a.out
